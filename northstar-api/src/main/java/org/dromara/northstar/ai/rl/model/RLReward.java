package org.dromara.northstar.ai.rl.model;

/**
 * 强化学习马可夫过程中的奖励
 * @auth KevinHuangwl
 */
public record RLReward(double value) {

}
